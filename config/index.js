var path = require('path')
module.exports = {
  build: {
    env: require('./.env')
  },
  buildStaging: {
    env: require('./.env'),
    // ...
  },
  buildProduction: {
    env: require('./.env'),
    // ...
  },
  devStaging: {
    env: require('./.env'),
    port: 8080,
    // ...
  }
}