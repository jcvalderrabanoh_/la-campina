module.exports = {
    NODE_ENV: '"production"',
    VUE_APP_BASE_URL: '"http://localhost:8080/"',
    MICROSERVICIOS_URL: 'https://logiaerp.herokuapp.com/'
}